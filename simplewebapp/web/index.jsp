<%-- 
    Document   : index
    Created on : Feb 21, 2018, 2:50:02 PM
    Author     : Nuaji
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Survey Bahasa Pemograman</title>
    </head>
    <body>
        <h1>Selamat Datang Di Survey Developer</h1>
        <p>Pilihlah Bahasa Pemrograman Apa yang Anda Kenal</p>
        <form action="ControllerServlet" method="post">
            <table border="0">
               <%--  <thead>
                    <tr>
                        <th></th>
                        <th></th>
                    </tr>
                </thead> --%>
                <tbody>
                    <tr>
                        <td>Nama Lengkap</td>
                        <td><input type="text" name="namaLengkap" value="" /></td>
                    </tr>
                    <tr>
                        <td>Java</td>
                        <td><input type="checkbox" name="bhsProgram" value="Java" /></td>
                    </tr>
                    <tr>
                        <td>ASP</td>
                        <td><input type="checkbox" name="bhsProgram" value="ASP" /></td>
                    </tr>
                    <tr>
                        <td>PHP</td>
                        <td><input type="checkbox" name="bhsProgram" value="PHP" /></td>
                    </tr>
                    <tr>
                        <td>Groovy</td>
                        <td><input type="checkbox" name="bhsProgram" value="Groovy" /></td>
                    </tr>
                    <tr>
                        <td>Visual Basic</td>
                        <td><input type="checkbox" name="bhsProgram" value="Visual Basic" /></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="submit" value="Kirim" /></td>
                    </tr>
                </tbody>
            </table>

        </form>
    </body>
</html>
