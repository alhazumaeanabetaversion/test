<%-- 
    Document   : outputlogin
    Created on : Feb 22, 2018, 9:39:01 AM
    Author     : Nuaji
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <jsp:useBean id="loginData" scope="request" class="astra.ac.id.model.LoginData" />
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Sukses</title>
    </head>
    <body>
        <h2>Welcome, <jsp:getProperty name="loginData" property="username" /></h2>
    </body>
</html>
