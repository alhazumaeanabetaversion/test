<%-- 
    Document   : output
    Created on : Feb 22, 2018, 8:13:03 AM
    Author     : Nuaji
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <jsp:useBean id="surveyData" scope="request" class="astra.ac.id.model.SurveyData" />
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Terima Kasih</title>
    </head>
    <body>
        <h2>Teima Kasih Untuk melakukan Survey Ini</h2>
        <p>
            <jsp:getProperty name="surveyData" property="fullName" />,
            Anda familiar dengan Bahasa Program Berikut :
        </p>
        <ul>
            <%
            String[] bahasa = surveyData.getProgLangList();
            if (bahasa != null){
                for(int i = 0;i<bahasa.length;i++){
            %>
            <li>
                <%= bahasa[i]%>
            </li>       
                <% }
            }
            %>
        </ul>
    </body>
</html>
